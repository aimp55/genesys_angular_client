'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:OauthClientDetailsCtrl
 * @description
 * # OauthClientDetailsCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('OauthClientDetailsCtrl', ['$scope', 'oauthClientService', 'userService','genesysClient', '$routeParams', '$route', '$location', function ($scope, oauthClientService, userService,genesysClient, $routeParams, $route, $location) {

    genesysClient.checkAuthentification();

    //id not clientId
    var id = $routeParams.id;

    var getClientDetail = function (id) {
      oauthClientService.getClientDetail(id).success(function (data) {
        $scope.clientData = data;
        getTokens(data.clientId)
      }).error(function (data) {
        console.log('Get client detail error ' + JSON.stringify(data));
      });
    };

    var getTokens = function (id) {
      oauthClientService.getTokens(id).success(function (data) {
        $scope.accessTokens = data.accessTokens;
        $scope.refreshTokens = data.refreshTokens;
        //try to get user info for current client from tokens
        if (data.accessTokens[0] != null) {
          getUser(data.accessTokens[0].userUuid);
        } else if (data.refreshTokens[0] != null) {
          getUser(data.refreshTokens[0].userUuid);
        }
      }).error(function (data) {
        console.log('Get client tokens error ' + JSON.stringify(data));
      });
    };

    var getUser = function (uuid) {
      userService.getUserByUuid(uuid).success(function (data) {
        $scope.user = data
      }).error(function (data) {
        console.log('Get user by uuid error ' + JSON.stringify(data));
      });
    };


    getClientDetail(id);


    $scope.removeAccessesToken = function (tokenId) {
      oauthClientService.removeAccessesToken(tokenId).success(function () {
        console.log('Access token was deleted');
        $route.reload(); //if its own Accesses token has been removed then you need to check authentification
      }).error(function (data) {
        console.log('Delete access token error ' + JSON.stringify(data));
      });
    };

    $scope.removeRefreshToken = function (tokenId) {
      oauthClientService.removeRefreshToken(tokenId).success(function () {
        console.log('Refresh token was deleted');
        getTokens($scope.clientData.clientId);
      }).error(function (data) {
        console.log('Delete refresh token error ' + JSON.stringify(data));
      });
    };

    $scope.removeAllAccessesTokens = function (clientId) {
      oauthClientService.removeAllAccessesTokens(clientId).success(function () {
        console.log('All access token were removed');
        $route.reload();
      }).error(function (data) {
        console.log('Delete All access token error ' + JSON.stringify(data));
      });
    };

    $scope.removeAllRefreshTokens = function (clientId) {
      oauthClientService.removeAllRefreshTokens(clientId).success(function () {
        console.log('All refresh token were removed');
        getTokens($scope.clientData.clientId);
      }).error(function (data) {
        console.log('Delete All refresh token error ' + JSON.stringify(data));
      });
    };

    $scope.editInfo = function(){
      $location.path('/oauthClient/edit/'+id);
    };

    $scope. editPermissions = function(){
      $location.path('/oauthClient/permissions/'+id);
    }


  }]);
