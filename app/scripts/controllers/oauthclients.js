'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:OauthclientsCtrl
 * @description
 * # OauthclientsCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('OauthClientsCtrl', ['$scope', '$location', 'genesysClient', 'oauthClientService', function ($scope, $location, genesysClient, oauthClientService) {

    genesysClient.setBrowsedPage('oauthClients');

    genesysClient.checkAuthentification();

    var getClients = function () {
      oauthClientService.getClients().success(function (data) {
        $scope.clients = data;
      }).error(function (data) {
        console.log('Get OAuth clients error ' + JSON.stringify(data));
      })
    };

    var selectClient = function(id){
      $location.path('/oauthClient/details/'+id);
    };

    getClients();

    $scope.addClient = function () {
      $location.path('/oauthClient/edit');
    };

    $scope.selectClient = function(id){
      selectClient(id)
    };




  }]);
