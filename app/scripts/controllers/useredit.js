'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:UsereditCtrl
 * @description
 * # UsereditCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('UserEditCtrl',['$routeParams', '$location','$scope','userService', 'genesysClient', function ($routeParams,$location, $scope, userService, genesysClient) {

    genesysClient.checkAuthentification();

    var id = $routeParams.id;

    var getUserDetail = function () {
      userService.getUserDetail(id).success(function (data) {
        $scope.userData = data.user;
      }).error(function (data) {
        console.log('Get user detail error ' + JSON.stringify(data));
      });
    };

    var getRoles = function(){
      userService.getAvailableRoles().success(function (data) {
        $scope.userRoles = data;
      }).error(function (data) {
        console.log('Get user Roles error ' + JSON.stringify(data));
      });
    };

    var userSave = function() {
      userService.setUserData($scope.userData).success(function () {
        $location.path('/user/'+id);
      }).error(function (data) {
        console.log('Set user Data error ' + JSON.stringify(data));
      });
    };

    var userSaveRoles = function() {
      userService.setUserRoles($scope.userData).success(function () {
        $location.path('/user/'+id);
      }).error(function (data) {
        console.log('Set user Data error ' + JSON.stringify(data));
      });
    };

    $scope.userSave = function(){
      userSave();
    };

    $scope.userSaveRoles = function(){
      userSaveRoles();
    };

    $scope.cancel = function(){
      $location.path('/user/'+id);
    };

    $scope.toggleCheck = function(role){

      if ($scope.userData.roles.indexOf(role) == -1){
        $scope.userData.roles.push(role);
      }else{
        var index = $scope.userData.roles.indexOf(role);
        $scope.userData.roles.splice(index, 1)
      }
    };

    getRoles();
    getUserDetail();



  }]);
