'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:OauthClientEditCtrl
 * @description
 * # OauthClientEditCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('OauthClientEditCtrl', ['$scope', '$routeParams', '$location', 'oauthClientService', 'genesysClient', function ($scope, $routeParams, $location, oauthClientService, genesysClient) {

    genesysClient.checkAuthentification();

    // id not clientId
    var id = $routeParams.id;
    $scope.clientData = {};

    var getClientDetail = function (id) {
      oauthClientService.getClientDetail(id).success(function (data) {
        $scope.clientData = data;
      }).error(function (data) {
        console.log('Get client detail error ' + JSON.stringify(data));
      });
    };

    var goBack = function(){
      if(id != undefined){
        $location.path('/oauthClient/details/' + id)
      }else{
        $location.path('/oauthClients')
      }
    };

    if (id != undefined) {
      getClientDetail(id);
    }

    $scope.saveClient = function () {
      oauthClientService.saveClient($scope.clientData, id).success(function () {
        goBack();
      }).error(function (data) {
        console.log('Save client  error ' + JSON.stringify(data));
      });
    };

    $scope.deleteClient = function () {
      oauthClientService.deleteClient(id).success(function () {
        $location.path('/oauthClients');
      }).error(function (data) {
        console.log('Client delete error ' + JSON.stringify(data));
      });
    };

    $scope.cancel = function () {
        goBack();
    };


  }]);
