'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('LoginCtrl', [ '$scope', '$http', 'genesysClient', function ( $scope, $http, genesysClient) {

    genesysClient.isAlreadyAuthenticated();

    $scope.authorize = function () {

      var url = genesysClient.composeAuthorizeUrl();

      // replace changes browser history, go adds to it
      window.location = url;

    };

  }]);

