'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:AuthorizationCtrl
 * @description
 * # AuthorizationCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('AuthorizationCtrl', ['$rootScope', '$http', '$location', 'genesysClient', function ($rootScope, $http, $location, genesysClient) {

    var query = genesysClient.getQueryParams(document.location.search);

    genesysClient.setAutorizeCode(query.code);

    var tokenUrl = genesysClient.composeTokenUrl();

    $http.get(tokenUrl)
      .success(function (data) {
        console.log('Got JSON response: ' + JSON.stringify(data));
        console.log('rt=' + data['refresh_token']);
        genesysClient.updateTokens(data);
        $location.path(genesysClient.getBrowsedPage());

      }).error(function (data) {
        console.log(data);
        console.log(data.error);
        $location.path('/login');
      });

  }]);
