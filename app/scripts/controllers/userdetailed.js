'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:UserdetailedCtrl
 * @description
 * # UserdetailedCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('UserDetailedCtrl', ['$scope', '$routeParams', '$location', 'userService', 'oauthClientService', 'genesysClient', '$modal', function ($scope, $routeParams, $location, userService, oauthClientService, genesysClient, $modal) {

    genesysClient.checkAuthentification();

    var id = $routeParams.id;

    var getUserDetail = function () {
      userService.getUserDetail(id).success(function (data) {
        $scope.userData = data.user;
        $scope.userTeams = data.userTeams;
        $scope.isVetted = $.inArray('VETTEDUSER', $scope.userData.roles) != -1;
      }).error(function (data) {
        console.log('Get user detail error ' + JSON.stringify(data));
      });
    };

    var sendEmail = function () {
      userService.sendEmail($scope.userData.uuid).success(function () {
        console.log('Mail was sent');
      }).error(function (data) {
        console.log('Sending mail error ' + JSON.stringify(data));
      });
    };

    var addRoleVettedUser = function () {
      userService.addRoleVettedUser($scope.userData.uuid).success(function () {
        console.log('Vetted role was added');
        getUserDetail();
      }).error(function (data) {
        console.log('Adding vetted role error ' + JSON.stringify(data));
      });
    };

    var userLock = function(uuid, lock){
      userService.setUserLocked(uuid, lock).success(function () {
        console.log('User lock was changed on\''+lock+'\'');
      }).error(function (data) {
        console.log('User lock/unlock error ' + JSON.stringify(data));
      });
    };

    var userEnable = function(uuid, enable){
      userService.setUserEnabled(uuid, enable).success(function () {
        console.log('User enable was changed on\''+enable+'\'');
      }).error(function (data) {
        console.log('User enable/disable error ' + JSON.stringify(data));
      });
    };

    var openModal = function () {
      $modal.open({
        templateUrl: 'views/widget.html',
        controller: 'WidgetCtrl'
      });
    };

    getUserDetail();

    $scope.userTeams = [];

    $scope.edit = function () {
      $location.path('/user/edit/' + id);
    };

    $scope.lock = function () {
      userLock($scope.userData.uuid, 'true');
    };

    $scope.unlock = function () {
      userLock($scope.userData.uuid, 'false');
    };

    $scope.disable = function () {
      userEnable($scope.userData.uuid, 'false');
    };

    $scope.enable = function () {
      userEnable($scope.userData.uuid, 'true');
    };

    $scope.send = function () {
      sendEmail();
    };

    $scope.addRoleVettedUser = function () {
      addRoleVettedUser();
    };

    $scope.getWidget = function () {
      openModal();
    };

  }]);

