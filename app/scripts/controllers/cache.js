'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:CacheCtrl
 * @description
 * # CacheCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp').controller('CacheCtrl', ['$http', '$scope','genesysClient', 'cacheService',function ($http, $scope,genesysClient, cacheService) {

  genesysClient.setBrowsedPage('cache');

  genesysClient.checkAuthentification();

    var getCache = function(){
      cacheService.getCache().success(function (data) {
        $scope.cacheMaps = data.cacheMaps;
      }).error(function (data){
        console.log('Get cash error ' + JSON.stringify(data));
      });
    };

    var clearTilesCache = function(){
      cacheService.clearTilesCache().success(function(){
        getCache();
      })
    };

    var clearAllCaches = function(){
      cacheService.clearAllCaches().success(function(){
        getCache();
      })
    };

    $scope.cacheMaps = [];

    $scope.getCache = function(){
      getCache();
    };

    $scope.clearTilesCache = function(){
      clearTilesCache();
    };

    $scope.clearAllCaches = function(){
      clearAllCaches();
    };

    getCache();

  }]);
