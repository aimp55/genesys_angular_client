'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:CacheCtrl
 * @description
 * # CacheCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp').controller('HomeCtrl', ['$rootScope','$http', '$scope','genesysClient', 'cacheService',function ($rootScope,$http, $scope,genesysClient, cacheService) {

  $scope.isAlresdyAutorized = genesysClient.localTokens().accessToken != null;

}]);
