'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:WidgetCtrl
 * @description
 * # WidgetCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('WidgetCtrl', ['$scope', 'genesysClient', 'oauthClientService', function ($scope, genesysClient, oauthClientService) {

    genesysClient.checkAuthentification();

    var getClients = function () {
      oauthClientService.getClients().success(function (data) {
        $scope.clients = data;
      }).error(function (data) {
        console.log('Get OAuth clients error ' + JSON.stringify(data));
      })
    };

    var getWidget = function () {
      oauthClientService.getWidget($scope.client.clientId).success(function (data) {
        $scope.widget = data;
      }).error(function (data) {
        console.log('Get Widget error ' + JSON.stringify(data));
      });
    };

    getClients();

    $scope.showWidget = false;

    $scope.clientSelect = function () {
      getWidget();
    };

    $scope.closeMV = function () {
      this.$dismiss('cancel');
    }

  }]);
