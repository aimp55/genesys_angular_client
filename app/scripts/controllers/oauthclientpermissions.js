'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:OauthClientPermissionsCtrl
 * @description
 * # OauthClientPermissionsCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('OauthClientPermissionsCtrl', ['$scope', '$route', '$routeParams', '$location', 'oauthClientService', 'userService', 'genesysClient', function ($scope, $route, $routeParams, $location, oauthClientService, userService, genesysClient) {

    genesysClient.checkAuthentification();

    var id = $routeParams.id;

    $scope.data = {};
    $scope.data.users = [];
    $scope.newPermission = {};
    $scope.newPermission.checkbox = [];

    $scope.permissionMask = {
      1: 'Read',
      2: 'Write',
      4: 'Create',
      8: 'Delete',
      16: 'Manage'
    };

    //Permissions checkboxes
    $scope.checkboxes = [];

    var fillCheckboxes = function () {
      angular.forEach($scope.data.aclSids, function (aclSide, aclIndex) {
        $scope.checkboxes[aclIndex] = [];
        angular.forEach($scope.data.aclPermissions, function (permission, permissionIndex) {
          $scope.checkboxes[aclIndex][permissionIndex] = $scope.data.aclEntries[aclSide.sid][permission.mask]
        });
      });
    };

    var getPermissions = function () {
      oauthClientService.getPermissions(id).success(function (data) {
        $scope.data = data;
        fillCheckboxes();
        $scope.ownerEmail = $scope.getMail(data.aclObjectIdentity.ownerSid.sid);
      }).error(function (data) {
        console.log('Get client permissions error ' + JSON.stringify(data));
      });
    };

    getPermissions();

    $scope.editMode = false;

    $scope.edit = function () {
      $scope.editMode = true;
    };

    $scope.cancel = function () {
      $scope.editMode = false;
    };

    $scope.goBack = function () {
      $location.path("/oauthClient/details/"+id);
    };

    $scope.save = function (index) {
      var object = {
        "oid": $scope.data.aclObjectIdentity.objectIdIdentity,
        "clazz": $scope.data.aclObjectIdentity.aclClass.aclClass,
        "uuid": $scope.data.aclSids[index].sid,
        "principal": true,
        "create": $scope.checkboxes[index][0],
        "read": $scope.checkboxes[index][1],
        "write": $scope.checkboxes[index][2],
        "delete": $scope.checkboxes[index][3],
        "manage": $scope.checkboxes[index][4]
      };

      oauthClientService.updatePermission(object).success(function () {
        console.log("Permission was updated");
        $route.reload();
      }).error(function (data) {
        console.log('Update permission error ' + JSON.stringify(data));
      });

    };

    $scope.add = function () {
      var object = {
        "oid": $scope.data.aclObjectIdentity.objectIdIdentity,
        "clazz": $scope.data.aclObjectIdentity.aclClass.aclClass,
        "uuid": $scope.newPermission.sid,
        "principal": true,
        "create": $scope.newPermission[0],
        "read": $scope.newPermission[1],
        "write": $scope.newPermission[2],
        "delete": $scope.newPermission[3],
        "manage": $scope.newPermission[4]
      };

      oauthClientService.addPermission(object).success(function () {
        console.log("Permission was added");
        $route.reload();
      }).error(function (data) {
        console.log('Add permission error ' + JSON.stringify(data));
      });

    };

    $scope.getMail = function (userUuid) {
      for (var i = 0; i < $scope.data.users.length; i++) {
        if ($scope.data.users[i].uuid == userUuid) return $scope.data.users[i].email;
      }
    };


  }]);
