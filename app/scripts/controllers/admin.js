'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:AdminCtrl
 * @description # AdminCtrl Controller of the myAppApp
 */
angular.module('myAppApp').controller('AdminCtrl',
  ['$scope', '$rootScope', '$location', '$http', '$route', '$routeParams', 'genesysClient', 'adminService', function ($scope, $rootScope, $location, $http, $route, $routeParams, genesysClient, adminService) {

    genesysClient.checkAuthentification();

    console.log('Have local tokens: ' + JSON.stringify(genesysClient.localTokens));

    $scope.accessToken = genesysClient.localTokens().accessToken;
    $scope.tokenType = genesysClient.localTokens().tokenType;
    $scope.scopeType = genesysClient.localTokens().scopeType;
    $scope.refreshToken = genesysClient.localTokens().refreshToken;

    $scope.getMe = function () {
      adminService.getMe().success(function (data) {
        window.alert('name: ' + data.name + '\n\r email:  ' + data.email + '\n\r roles:  ' + data.roles);
      });
    };

    $scope.dropAuthorization = function () {
      genesysClient.resetLocalStorage();
      $route.reload();
    };

    $scope.goToLogin = function () {
      $location.path('/login');
    };

    $scope.refreshAccessToken = function () {
      genesysClient.refreshAccessToken();
      $scope.accessToken = genesysClient.localTokens().accessToken;
    };

  }]);
