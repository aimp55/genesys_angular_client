'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('UsersCtrl', ['$scope','$location', 'userService', 'genesysClient', function ($scope,$location, userService, genesysClient ) {

    genesysClient.setBrowsedPage('users');

    genesysClient.checkAuthentification();

    $scope.content = [];

    var getUsers = function () {
      userService.getUsers().success(function (data) {
        $scope.content = data.content;
      }).error(function (data) {
        console.log('Get users error ' + JSON.stringify(data));
      });
    };

    $scope.getUsers = getUsers();

    $scope.selectUser = function (id) {
      $location.path('/user/'+id);
    };

    getUsers();

  }]);
