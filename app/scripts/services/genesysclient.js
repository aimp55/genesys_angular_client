'use strict';

/**
 * Genesys Client API configuration and authentication, also a place to sign the
 * request with OAuth access token.
 *
 * @ngdoc service
 * @name myAppApp.genesysClient
 * @description # genesysClient Service in the myAppApp.
 */
angular.module('myAppApp').service('genesysClient', ['$http', '$rootScope', '$location', '$localStorage', function ($http, $rootScope, $location, $localStorage) {

  $localStorage.$default({
    oauthTokens: {
      accessToken: null,
      refreshToken: null
    }
  });

  // Constants
  var grantType = 'authorization_code';
  var grantTypeRefresh = 'refresh_token';
  var responseType = 'code';

  // Configurable stuff
  var webAppUrl = 'http://localhost:9000/';
  var baseUrl = 'http://localhost:8080';
  var authorizeUrl = '/oauth/authorize';
  var tokenUrl = '/oauth/token';
  var clientId = 'JxKLD.VYSCNVrETVq0b0dY1oGj@localhost';
  var scopeType = 'read,write';
  var clientSecret = 'r9BLwob6LwhlzlLOCRaHqrHP6qeW7MGP';
  var redirectAuthUrl = webAppUrl + '#/authorization';

  // code from Authorization Response
  var autorizeCode = null;

  var localTokens = $localStorage.oauthTokens;

  var pages = {
    cache: '/cache',
    logger: '/loggers',
    users: '/users',
    oauthClients: "/oauthClients",
    home: '/home'
  };

  var oauthHeaders = function () {
    return {
      'Authorization': 'Bearer ' + localTokens.accessToken
    };
  };

  var composeRefreshTokenUrl = function () {
    var requestParams = {};
    requestParams['client_id'] = clientId;
    requestParams['client_secret'] = clientSecret;
    requestParams['grant_type'] = grantTypeRefresh;
    requestParams['refresh_token'] = localTokens.refreshToken;
    requestParams['redirect_uri'] = redirectAuthUrl;

    return baseUrl + tokenUrl + '?' + $.param(requestParams);
  };

  var genesysClient = {

    getAppUrl: function () {
      return webAppUrl;
    },

    setBrowsedPage: function (name) {
      $localStorage.browsedPage = pages[name];
    },

    getBrowsedPage: function () {
      return $localStorage.browsedPage != undefined ? $localStorage.browsedPage : pages.home;
    },

    localTokens: function(){
      return localTokens;
    },

    getRestApiUrl: function () {
      return baseUrl + '/api/v0'
    },

    composeTokenUrl: function () {
      var requestParams = {};
      requestParams['client_id'] = clientId;
      requestParams['client_secret'] = clientSecret;
      requestParams['grant_type'] = grantType;
      requestParams['code'] = autorizeCode;
      requestParams['redirect_uri'] = redirectAuthUrl;

      return baseUrl + tokenUrl + '?' + $.param(requestParams);
    },

    composeAuthorizeUrl: function () {
      var requestParams = {};
      requestParams['client_id'] = clientId;
      requestParams['client_secret'] = clientSecret;
      requestParams['response_type'] = responseType;
      requestParams['redirect_uri'] = redirectAuthUrl;

      return baseUrl + authorizeUrl + '?' + $.param(requestParams);
    },

    getQueryParams: function (qs) {
      qs = qs.split('+').join(' ');

      var params = {}, re = /[?&]?([^=]+)=([^&]*)/g;
      var x = null;
      while (x = re.exec(qs)) {
        params[decodeURIComponent(x[1])] = decodeURIComponent(x[2]);
      }

      return params;
    },

    resetLocalStorage: function () {
      $localStorage.$reset({
        oauthTokens: {
          accessToken: null,
          refreshToken: null
        }
      });
      localTokens = $localStorage.oauthTokens;
    },

    setAutorizeCode: function (code) {
      autorizeCode = code;
    },

    requestHeaders: function () {
      return {
        headers: oauthHeaders()
      };
    },

    updateTokens: function (tokens) {
      localTokens.accessToken = tokens['access_token'];
      localTokens.refreshToken = tokens['refresh_token'];
      localTokens.tokenType = tokens['token_type'];
      localTokens.scopeType = tokens['scope'];
      // Save as 'oauthTokens'
      localStorage.oauthTokens = localTokens;
    },

    // Use refreshToken to get new accessToken
    refreshAccessToken: function () {
      var url = composeRefreshTokenUrl();
      // Get new token
      $http.get(url).success(function (data) {
        console.log('Got new tokens ' + JSON.stringify(data));
        genesysClient.updateTokens(data);
      }).error(function () {
        console.log('could not get access token with refresh token, need to fully re-authorize');
        genesysClient.resetLocalStorage();
        $location.path('/login');
      });
    },

    checkAuthentification: function () {
      if (localTokens.accessToken === null) {
        // If we have no access token, go to login
        $location.path('/login');
      } else {
        $http.get(genesysClient.getRestApiUrl() + '/me').success(function () {
          // All okay! do nothing
        }).error(function (data) {
          if ((data.error === 'invalid_token') && (localTokens.refreshToken !== null)) {
            //Go to get access token with existing refresh token
            genesysClient.refreshAccessToken();
          } else {
            console.log(data.error);
            $location.path('/login');
          }
        });
      }
    },

    /*
     * check if access_token is valid if not and refresh token present then try
     * to get new access_token and redirect to authorization controller
     */
    isAlreadyAuthenticated: function () {
      console.log('Checking if access- and refreshToken are still valid');

      if (localTokens.accessToken !== null || localTokens.refreshToken !== null) {
        console.log('Got a copy of access- or refreshToken. Checking against server.');

        $http.get(genesysClient.getRestApiUrl() + '/me').success(function () {
          console.log('You have already authorized');
          $location.path('/admin');

        }).error(function () {
          if (localTokens.refreshToken !== null) {
            genesysClient.refreshAccessToken();
          }
        });
      }
    }

  };

  return genesysClient;

}]);
