'use strict';

/**
 * Genesys Cash API
 *
 * @ngdoc service
 * @name myAppApp.cashService
 * @description # cashService Service in the myAppApp.
 */
angular.module('myAppApp').service('cacheService', [ '$http', 'genesysClient', function($http, genesysClient) {

  return {
    getCache : function() {
      return $http.get(genesysClient.getRestApiUrl()+'/cache');
    },

    clearTilesCache : function() {
      return $http.post(genesysClient.getRestApiUrl()+'/cache/clearTilesCache',{});
    },

    clearAllCaches : function() {
      return $http.post(genesysClient.getRestApiUrl()+'/cache/clearCaches',{});
    }

  }
} ]);
