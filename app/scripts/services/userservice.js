'use strict';

/**
 * Genesys User API
 *
 * @ngdoc service
 * @name myAppApp.userService
 * @description # userService Service in the myAppApp.
 */
angular.module('myAppApp').service('userService', ['$http', 'genesysClient', function ($http, genesysClient) {


  var restIp = genesysClient.getRestApiUrl();

  var usersRestIpi = restIp + '/users';

  return {

    getUsers: function () {
      return $http.get(usersRestIpi);
    },

    getAvailableRoles: function () {
      return $http.get(usersRestIpi + '/available_roles');
    },

    getUserDetail: function (id) {
      return $http.get(usersRestIpi + '/user/' + id);
    },

    getUserByUuid: function (id) {
      return $http.get(usersRestIpi + '/user/uuid/' + id);
    },

    getTeams: function () {
      return $http.get(restIp + '/me/teams')
    },

    setUserLocked: function (uuid, locked) {
      return $http.post(restIp + '/user/' + uuid + '/locked', locked)
    },

    setUserEnabled: function (uuid, enebled) {
      return $http.post(restIp + '/user/' + uuid + '/enabled', enebled)
    },

    setUserData: function (userData) {
      return $http.post(usersRestIpi + '/user/data', {
        "uuid": userData.uuid,
        "name": userData.name,
        "email": userData.email,
        "pwd1": userData.pwd1,
        "pwd2": userData.pwd2
      });

    },

    setUserRoles: function (userData) {
      return $http.post(usersRestIpi + '/user/roles', {
        "uuid": userData.uuid,
        "roles": userData.roles
      });
    },

    sendEmail: function(userUuid){
      return $http.get(usersRestIpi + '/user/'+userUuid+'/send');
    },

    addRoleVettedUser: function(userUuid){
      return $http.get(usersRestIpi + '/user/'+userUuid+'/vetted-user');
    }
  };

}]);
