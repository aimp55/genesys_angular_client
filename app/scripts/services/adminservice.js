'use strict';

/**
 * Service for Genesys Admin calls
 *
 * @ngdoc service
 * @name myAppApp.adminService
 * @description # adminService Service in the myAppApp.
 */
angular.module('myAppApp').service('adminService', ['$http', 'genesysClient', function ($http, genesysClient) {

  return {

    getMe: function () {
      return $http.get(genesysClient.getRestApiUrl() + '/me');
    }

  };
}]);
