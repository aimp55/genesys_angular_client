'use strict';

/**
 * Genesys oauth client API
 *
 * @ngdoc service
 * @name myAppApp.oauthClientService'
 * @description # oauthClientService' Service in the myAppApp.
 */
angular.module('myAppApp').service('oauthClientService', ['$http', 'genesysClient', function ($http, genesysClient) {

  var url = genesysClient.getRestApiUrl() + "/oauth";

  return {
    getClients: function () {
      return $http.get(url + '/clientslist');
    },

    getClientDetail: function (id) {
      return $http.get(url + "/" + id);
    },

    getTokens: function (clientId) {
      return $http.get(url + "/tokens/" + clientId);
    },

    removeAccessesToken: function (tokenId) {
      return $http.get(url + "/token/at/" + tokenId + "/remove");
    },

    removeRefreshToken: function (tokenId) {
      return $http.get(url + "/token/rt/" + tokenId + "/remove");
    },

    removeAllAccessesTokens: function (clientId) {
      return $http.get(url + "/token/" + clientId + "/remove-all-at");
    },

    removeAllRefreshTokens: function (clientId) {
      return $http.get(url + "/token/" + clientId + "/remove-all-rt");
    },

    saveClient: function (clientData, id) {
      return $http.post(url + '/save-client', {
        id: id,
        title: clientData.title,
        description: clientData.description,
        clientSecret: clientData.clientSecret,
        redirectUris: clientData.redirectUris,
        accessTokenValiditySeconds: clientData.accessTokenValiditySeconds,
        refreshTokenValiditySeconds: clientData.refreshTokenValiditySeconds
      });
    },

    deleteClient: function (id) {
      return $http.post(url + '/delete-client', {
        id: id
      });
    },

    getPermissions: function (id) {
      return $http.get(url + "/permissions/" + id);
    },

    updatePermission: function (object) {
      return $http.post(genesysClient.getRestApiUrl() + "/permission/update", object);
    },

    addPermission: function (object) {
      return $http.post(genesysClient.getRestApiUrl() + "/permission/add", object);
    },

    getWidget: function(clientId){
      return $http.get(url + "/client/"+clientId+"/get_widget");
    }

  }

}]);
