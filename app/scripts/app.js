'use strict';

/**
 * @ngdoc overview
 * @name myAppApp
 * @description # myAppApp Main module of the application.
 */
angular.module('myAppApp', ['ngAnimate', 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize', 'ngTouch', 'ngStorage','ui.bootstrap']).config(function ($routeProvider) {
  $routeProvider.when('/', {
    redirectTo : '/login'
  }).when('/login', {
    templateUrl : 'views/login.html',
    controller : 'LoginCtrl',
    resolve: {
      // I will cause a 0,5 second delay
      delay: function($q, $timeout) {
        var delay = $q.defer();
        $timeout(delay.resolve, 500);
        return delay.promise;
      }
    }
  }).when('/home', {
    templateUrl : 'views/home.html',
    controller : 'HomeCtrl'
  }).when('/cache', {
    templateUrl : 'views/cache.html',
    controller : 'CacheCtrl'
  }).when('/users', {
    templateUrl : 'views/users.html',
    controller : 'UsersCtrl'
  }).when('/user/:id', {
    templateUrl : 'views/userDetailed.html',
    controller : 'UserDetailedCtrl'
  }).when('/user/edit/:id', {
    templateUrl : 'views/userEdit.html',
    controller : 'UserEditCtrl'
  }).when('/oauthClients', {
    templateUrl : 'views/oauthClients.html',
    controller : 'OauthClientsCtrl'
  }).when('/oauthClient/details', {
    templateUrl : 'views/oauthClientDetails.html',
    controller : 'OauthClientDetailsCtrl'
  }).when('/oauthClient/details/:id', {
    templateUrl : 'views/oauthClientDetails.html',
    controller : 'OauthClientDetailsCtrl'
  }).when('/oauthClient/edit', { //for adding new
    templateUrl : 'views/oauthClientEdit.html',
    controller : 'OauthClientEditCtrl'
  }).when('/oauthClient/edit/:id', { //for editing
    templateUrl : 'views/oauthClientEdit.html',
    controller : 'OauthClientEditCtrl'
  }).when('/oauthClient/permissions/:id', {
    templateUrl : 'views/oauthClientPermissions.html',
    controller : 'OauthClientPermissionsCtrl'
  }).when('/admin', {
    templateUrl : 'views/admin.html',
    controller : 'AdminCtrl'
  }).when('/authorization', {
    templateUrl : 'views/authorization.html',
    controller : 'AuthorizationCtrl'
  }).otherwise({
    redirectTo : '/home'
  });
}).config(['$httpProvider', function ($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]).run(['$injector', 'genesysClient', function ($injector, genesysClient) {
    $injector.get("$http").defaults.transformRequest = function (data, headersGetter) {
      if (genesysClient.localTokens()) headersGetter()['Authorization'] = "Bearer " + genesysClient.localTokens().accessToken;
      if (data) {
        return angular.toJson(data);
      }
    };
  }]);
