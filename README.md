# Genesys angular client

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Load dependencies

Run 'npm install'  and  'bower install' to load project dependencies

## Server 

Change the constants for the server in app.js

## Build & development

Run `grunt` for building and `grunt serve` for preview.

