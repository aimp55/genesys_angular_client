'use strict';

describe('Service: genesysClient', function () {

  // load the service's module
  beforeEach(module('myAppApp'));

  // instantiate service
  var genesysClient;
  beforeEach(inject(function (_genesysClient_) {
    genesysClient = _genesysClient_;
  }));

  it('should do something', function () {
    expect(!!genesysClient).toBe(true);
  });

});
